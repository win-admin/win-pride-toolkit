---
layout: default
title: Home
nav_order: 1
description: "Home landing page"
has_children: false
---

<!-- ![WIN-logo](docs/img/WIN-h100.png) -->

# Wellcome Centre for Integrative Neuroimaging LGBTQIA+ Toolkit
{: .fs-8 }
A collection of LGBTQIA+ related resources to build an informed and inclusive community.
{: .fs-6 .fw-300 }


---

**Our science is better if we are diverse, our workplace is better when we are inclusive.**

The [Wellcome Centre for Integrative Neuroimaging (WIN)](https://www.win.ox.ac.uk) recognises LGBTQIA+ people are an integral part of our community. We aim to create a workplace where individuals of all sexual orientations, gender identities, and expressions feel valued and included. We aspire that all members of our community feel comfortable to be themselves at WIN.

More information about EDI at WIN can be found [here](https://www.win.ox.ac.uk/about/equality-diversity-and-inclusion).


## What does LGBTQIA+ stand for? 
LGBT is an acronym that abbreviates lesbian, gay, bisexual and transgender people. Q stands for queer or questioning, I for intersex and A for asexual. The "+" indicates the inclusion of non-binary folk and the many other ways in which people choose to express or define their sexuality and/or gender identity. With this acronym we intend for all members of the community to be included and valued. Unsure what these terms mean? Check out our terms and definitions section to get up to date on language.

*–Adapted from the Oxford University LGBTQ+ Society*


## Who is this toolkit for?
A core component of our mission at WIN is to cultivate a community environment that is informed. This toolkit is a collection of resources for **all** WIN members to benefit from. 
- Interested in being part of the LGBTQIA+ community? Check out our community page [here](https://open.win.ox.ac.uk/pages/win-admin/win-pride-toolkit/docs/community/community/).
- Want to learn more about how you can support your LGBTQIA+ friends and colleagues? Check out our page [here](https://open.win.ox.ac.uk/pages/win-admin/win-pride-toolkit/docs/supporting-your-peers/supporting-your-peers/).
- Get in touch? Find out how to connect with us [here](https://open.win.ox.ac.uk/pages/win-admin/win-pride-toolkit/docs/contact/contact/)
- Want to read more about the current status of LGBTQIA+ in science, the workplace and more? See our collection of resources [here](https://open.win.ox.ac.uk/pages/win-admin/win-pride-toolkit/docs/resources/resources/).

**Use the sidebar to explore more.**


## Suggestions?
The material is collected and maintained by our [WIN Pride member network](https://www.win.ox.ac.uk/about/equality-diversity-and-inclusion/member-networks/win-pride-member-network). It is not exhaustive and is a living document that we aim to keep updating. To can get in touch with WIN Pride or suggest new content, email [win-pride-queries@ndcn.ox.ac.uk](mailto:win-pride-queries@ndcn.ox.ac.uk)


## Legal protections
WIN and the University of Oxford recognise UK legislation (Equality Act 2010 and Gender Recognition Act 2004) in which people are [legally protected](https://edu.admin.ox.ac.uk/legal-protection) against discrimination, harassment or victimisation related to gender reassignment or sexual orientation, both of which are defined as 'protected characteristics'. The Gender Recognition Act 2004 is a UK law that allows individuals to change their gender and asserts their right to be recognised in their confirmed gender. The University of Oxford Equality Statement is available [here](https://edu.admin.ox.ac.uk/equality-policy). 



