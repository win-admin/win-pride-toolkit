# WIN-Pride-toolkit

## Description
Back end for generating an open-WIN webpage hosting the WIN Pride Toolkit: A collection of LGBT+ related resources to build an informed and inclusive community.

This page is contributed to by members of WIN Pride. We welcome suggestions for edits or additional material from the wider community. However, for consistent messaging, select members of the WIN community are developers. Edits must be ok'ed by WIN admin/management.

This repo is based on the open WIN community gitlab https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/.

## How to contribute

Pages can be updated by editing the markdown (*.md) files. The landing page can be edited via index.md with other pages found in in docs/

Changes can be made by either making a local copy of this repo using git clone and pushing edits to online repo:
```
  git clone git@git.fmrib.ox.ac.uk:win-admin/win-pride-toolkit.git
  git checkout -b my-new-branch
  # make changes to files
  git add .
  git commit -m "my commit message"
  git push origin my-new-branch
  # Review merge request with WIN Pride and admin/management
```

Or using the git web IDE.

Anything more than very minor edits should be made on a seperate branch and only added to main after revision by both other members of the WIN Pride Community, and WIN admin/management.

The online webpage will automatically update (you may need to wait several minutes for changes to come into affect).

## Tips for collaborating as a team

Some (potentially) helpful reminders for how to collaborate as a team on git:

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)



