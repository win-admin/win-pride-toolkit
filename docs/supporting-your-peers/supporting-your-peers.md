---
layout: default
title: Supporting Your Peers
has_children: false
nav_order: 5
---

# Supporting Your LGBTQIA+ Peers
{: .fs-9 }

Practical advice on what you can do to support the LGBTQIA+ community.
{: .fs-6 .fw-300 }

See below for sections including:
- Becoming a better ally
- Line manager advice
- Respecting pronouns: a guide
- Training programmes and courses

---

## Becoming a better ally

Allyship is the lifelong process in which people with privilege and power work to develop empathy towards another marginalised group's challenges or issues. Being an ally requires continuous action on the part of the individual to listen, to recognise, and support others. We can all raise each other up and work to foster a more inclusive and equitable environment for others. Allyship is hugely important to equality, inclusivity and justice - without allies to support marginalised groups, and empower others, the world would be a lonely place for many.

![allyship](../../../docs/supporting-your-peers/allyship-icon.png)
*copied from the LGBT Foundation https://lgbt.foundation/help/our-training-academy/*

**The role of an ally is as diverse as the community it serves. While not a comprehensive list, some of these roles may include:**
- Being able to listen, and shine a spotlight on those whose voices are often unheard.
- Recognising your own privilege and power, and using that privilege to lift others up.
- Awareness of implicit biases you might have, acknowledging that you're not perfect but taking opportunities to learn and grow.
- Stretching and stepping outside of your comfort zone to speak up for others: in-front of them, but even if the marginalised person(s) are not there to hear the problematic language you challenge. Challenging accepted group dynamics and recognising that being part of the in-crowd should not supersede the importance of supporting individuals that are outsiders or that lack a voice in the in-crowd
- Supporting the group you're allying by letting them speak for themselves whenever possible.
- Not expecting your queer peers to want to speak on behalf of the community just because they’re part of it.
- Not expecting special recognition for being an ally, and not taking credit for the ideas of the marginalised group.


**The Coin Model of Privilege and Critical Allyship**

*[Nixon, S.A. The coin model of privilege and critical allyship: implications for health. BMC Public Health 19, 1637 (2019)](https://doi.org/10.1186/s12889-019-7884-9)*

The Coin Model of Privilege and Critical Allyship is a useful tool for understanding the nuances of privilege and the responsibilities that come with being an ally. Privileges are conceptualized as “coins” that are not earned but are rather granted by society based on these identity markers like race, gender, class, sexuality, ability, etc. The more coins you have, the more social power and opportunities you possess. People can have coins from multiple categories (e.g., a white, able-bodied male has coins in multiple areas) but may lack coins in others (e.g., being gay might reduce coins in another area). As an ally, the goal is to use your privilege (or coins) to support and uplift marginalized groups by leveraging your societal advantages to redistribute power and challenge systemic oppression. 

This model emphasizes that effective allyship is not about charity or pity but about solidarity, justice, and equity.
Find out more via the article above.

---

## Line manager advice

**Has someone disclosed some aspect of their identity to you?**

First of all, it's great that your peer has felt comfortable to talk to you about this. Now you need to think about what you can do to actively support them and continue to earn the privilege of their confidence. Here are our three simple steps to get started:

- **Listen:** Let your peer lead a conversation about how to interact with them. Don't make assumptions and act in good faith.
- **Get informed:** Next, its your time to put in some work to get informed. Re-use the words that an individual uses about themselves. There are some great resources on the web and links from this website. Remember, it is not the job of your colleague to educate you.
- **Ask and act:** Later ask how you can support them in their academic journey. Listen, then act.

Importantly, remember that your peer has spoken to you in confidence. Treat any conversations or disclosures as strictly confidential.

---

## Respecting pronouns: a guide

Using the correct pronouns for someone is a fundamental way to show respect and affirm their identity. Here’s how to do it right:

- **Ask and Share:** When meeting someone new, it’s good practice to ask for their pronouns and share yours. You can say, "Hi, I'm [Your Name], and I use [Your Pronouns]. What about you?" Normalising this exchange creates an inclusive environment.

- **Use Correct Pronouns Consistently:** Once you know someone’s pronouns, make sure to use them consistently in all interactions. This includes using the correct pronouns when referring to them in conversation or in written communication.

- **Correct Mistakes Gracefully:** If you accidentally use the wrong pronouns, apologise briefly, correct yourself, and move on. For example, "She—sorry, they—will be joining us later." Lingering on the mistake can make the situation more uncomfortable.

- **Respect Privacy:** Not everyone may be out in every space. If someone shares their pronouns with you, use them when appropriate but be mindful not to out someone in settings where they may not be safe.

- **Educate Yourself:** Familiarise yourself with different pronouns, including gender-neutral ones like they/them and others. Understanding and respecting a variety of pronouns helps foster a more inclusive community.

- **Lead by Example:** If you’re in a position to do so, advocate for pronoun usage in group settings. Encourage others to include their pronouns in email signatures, online profiles, and introductions.

Remember, using the correct pronouns is a simple yet powerful way to support and affirm someone’s identity. It’s not just about grammar — it’s about respect and dignity.

---

## Training programmes and courses

Oxford's Medical Science Division runs an inclusive leadership programme to train Principal Investigators (PIs) in becoming more inclusive, transparent, and supportive leaders. Find out how to get involved [here](https://www.win.ox.ac.uk/about/training/inclusive-leadership-programme).

Check out these [practical tips and examples](https://edu.admin.ox.ac.uk/how-you-can-support-trans-inclusion) for how you can support trans inclusion at the University of Oxford.

The Mathematical, Physical and Life Sciences Division (MPLS) provide an ongoing ED&I [training programme](https://www.mpls.ox.ac.uk/equality-diversity-and-inclusion-in-mpls/mpls-ed-i-training-programme). At the time of writing (Jan 2024) sessions included:
- Intersectional allyship
- Supporting neurodivergent staff
- Supporting disabled staff
- Being an effective bystander
- Anti-racist allyship
- Cultivating resilience and self-care for ourselves and other
- Creating an inclusive research culture

Sessions are delivered online and are open to staff from across the University. More information [here](https://www.mpls.ox.ac.uk/equality-diversity-and-inclusion-in-mpls/mpls-ed-i-training-programme)


