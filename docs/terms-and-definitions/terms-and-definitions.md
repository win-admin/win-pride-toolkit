---
layout: default
title: Terms and Definitions
has_children: false
nav_order: 6
---


# Terms and Definitions
{: .fs-9 }

There are different aspects to someone’s gender & sexual identity, and you may have heard several different words being used. It’s really important to use the correct language, but we shouldn’t let the fear of saying something wrong act as a barrier to engagement. Get clued up, act in good faith and join the conversation. 

See below for sections including:
- Understanding gender identity and sexual orientation
- Key terms and glossaries
- Understanding different pronouns

---

## Understanding gender identity and sexual orientation

**Gender identity** describes a person’s innate sense of their own gender, which may or may not correspond to the sex assigned at birth.

**Sexual or romantic orientation** describes a person's attraction to other people. This attraction may be sexual and/or romantic. These terms refers to a person's sense of identity based on their attractions, or lack thereof.
Orientations include, but are not limited to, lesbian, gay, bi, ace and straight.

*Definitions taken from [Stonewall](https://www.stonewall.org.uk/list-lgbtq-terms)*

**The Gender Unicorn** from TSER gives an overview of how different aspects of someone’s gender and sexual identity are different features which can come together in many ways.

![Gender Unicorn](../../../docs/terms-and-definitions/gender-unicorn.jpeg)

<b>Once you understand the gender unicorn, you understand that these can be related in many ways. Note that all identities include attraction to trans men/women or non-binary people where appropriate, e.g. gay men can be attracted to trans men.</b>
This is both gender and sexuality affirming. People may change their sexual identity if they are attracted to a non-binary individual or may not, depending on the cultural and personal significance of a term to them. E.g. a lesbian woman attracted to a non-binary person may choose to redefine themselves as bisexual/pansexual, or keep the label lesbian as they feel strongly tied to the lesbian community.

---

## Key terms and glossaries

Ever wondered what LGBTQIA+ stand for? See below.
*(mostly taken from the Stonewall resource below)*

- **Lesbian:** Refers to a woman who is attracted to women. Some non-binary people may also identify with this term.
- **Gay:** Refers to a man who is attracted to men. Also a generic term for lesbian and gay sexuality – some women define themselves as gay rather than lesbian. Some non-binary people may also identify with this term.
- **Bi:** An umbrella term for people who are attracted not only to one gender. Bisexual people are attracted to more than one gender, and pansexual people are attracted to all genders, or to people regardless of gender.
- **Cisgender or cis:** Someone whose gender is the same as that they were assigned at birth.
- **Transgender or trans:** An umbrella term to describe people whose gender is not the same as, or does not sit comfortably with, the sex they were assigned at birth. This broad term can be used to describe anyone who isn't cis, and is inclusive of a range of identities including trans man and trans woman. Some non-binary people also define as trans.
- **Non-binary**: An umbrella term for people whose gender is not comfortably defined within the gender binaries of ‘man’ or ‘woman’. Non-binary identities are varied and can include people who identify with some aspects of binary identities, while others reject them entirely.
- **Queer:** A term used by those wanting to reject specific labels of sexual orientation and/or gender identity. It is sometimes used as an umbrella term to cover all those who are not straight or cis. Although some LGBT people view the word as a slur, it was reclaimed in the late 80s by the queer community who have embraced it. (Q can also stand for questioning, which is when people are exploring their own sexual orientation and/or gender identity.)
- **Intersex:** A term used to describe a person who may have the biological attributes of both sexes or whose biological attributes do not fit with societal assumptions about what constitutes male or female. Intersex people may identify as male, female or non-binary.
- **Asexual:** A person who experiences little or no sexual attraction to others. Some asexual people experience romantic attraction, while others do not. Asexual people who experience romantic attraction might also use terms such as gay, bi, lesbian, straight and queer in conjunction with asexual to describe the direction of their romantic attraction.
- **+:** The "+" at the end is an inclusive symbol, acknowledging that there are numerous identities and orientations that may not be explicitly represented by the letters. It encompasses various gender identities, expressions, and sexual orientations beyond those listed in the acronym.


Language is important and there are many excellent online resources. Here are two of our favourites to get you started:
- [The ABCs of LGBTQIA+](https://www.nytimes.com/2018/06/21/style/lgbtq-gender-language.html) by the New York Times
- [List of LGBTQ+ terms](https://www.stonewall.org.uk/list-lgbtq-terms) by Stonewall

Note, these lists are non-exhaustive and the use of language is constantly evolving. Nevertheless, equipping yourself with a basic understanding of the common terms will help you feel more informed and understand when people have different identities.

---

## Understanding different pronouns

Pronouns are words used to refer to someone without using their name. People use different pronouns based on their gender identity, and respecting these pronouns is essential for creating an inclusive and supportive environment. Here are some common pronouns and how to use them:

1. He/Him/His <br>
Example: "He is going to the store. I’ll ask him if he needs help." <br>
Used by: Typically by people who identify as male, but it’s important to confirm. 

2. She/Her/Hers <br>
Example: "She is writing an article. I’ll help her with the edits." <br>
Used by: Typically by people who identify as female, but always ask to be sure.

3. They/Them/Their <br>
Example: "They are on their way. I’ll text them the details." <br>
Used by: Often by people who identify as non-binary, genderqueer, or when someone’s gender isn’t known. "They" can be singular or plural.

4. Ze/Zir/Zirs (or Xe/Xem/Xyrs) <br>
Example: "Ze is presenting today. I’ll ask zir if zir slides are ready." <br>
Used by: Some people who are non-binary or gender nonconforming may use these pronouns. They offer a gender-neutral alternative to he/she.

5. Other Pronouns <br>
Examples: Ne/nem/nirs, Ey/em/eirs, Ve/ver/vis <br>
Used by: These and other pronouns are used by people who feel that common pronouns do not adequately reflect their gender identity.

6. No Pronouns <br>
Example: "Alex is designing the website. I’ll help Alex with the coding." <br>
Used by: Some individuals prefer not to use pronouns at all and instead use their name in place of pronouns.

A guide on how to respect someone's pronouns if provided on the Supporting Your Peers page.





