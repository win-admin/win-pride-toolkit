---
layout: default
title: Community
has_children: false
nav_order: 4
---


# Community
{: .fs-9 }

Resources for LGBTQIA+ staff and students at WIN
{: .fs-6 .fw-300 }

See below for sections including:
- LGBTQIA+ networks at Oxford
- Community resources
- LGBTQIA+ champions at Oxford and WIN

---

## LGBTQIA+ networks at Oxford
Oxford has a vibrant LGBTQIA+ community. Below are some groups you might like to connect with.

[WIN Pride](https://www.win.ox.ac.uk/about/equality-diversity-and-inclusion/member-networks/win-pride-member-network) is our local network for LGBTI+ colleagues and allies. ​​We hold regular, informal meet-ups to chat about LGBTI+ themes (and more) in a safe and accepting environment. We have hosted LGBTI+ pub quizzes, organised WIN representation at Pride marches, led an informal 'info scavenger hunt' for LGBTQIA+ History Month, and promoted events focusing on LGBTQIA+ in STEM. We generally organise over the WIN-EDI Slack. Please get in touch with one of our LGBTQIA+ Champions below for more information.

[The Oxford LGBTQ+ Society](https://www.oulgbtq.org/) is a large and active student-run society at the University of Oxford. They run a wide range of events every week including pizza nights, Bob Ross paint-alongs, drinks, pumpkin carving, talks from guest speakers, welfare brunches, etc. some of which are identity based, such as ‘Ace/Aro Tea’. Their website includes many great welfare and identity resources (see below) as well as info about upcoming events. You can sig-up to their (confidential) mailing list via their website.

[The LGBTQIA+ Staff Network](https://edu.admin.ox.ac.uk/lgbt-staff-network) brings together LGBTQIA+ employees of the University of Oxford, Oxford University Press, colleges, nurseries and postgraduate students. The network uses an email list and teams channel to send out news relevant to LGBTQIA+ staff and holds various meetings throughout the year, both social and work-related.

[TransOxford](https://www.transoxford.org.uk/): an independent support group for transgender and gender non-conforming people in Oxfordshire. Members come from all walks of life and benefit from an informal exchange of social and professional ideas, knowledge, expertise and experience in many fields, including personal confidence and development, social integration in a new gender role, legal, psychological and other aspects of living with their gender identity.

[Oxford Pride](https://oxfordpride.uk/) are a charity run by a dedicated team of volunteers who hold events and collaborate with other organisations throughout the year to create safe, welcoming, and inclusive spaces for the LGBTQIA+ community. The highlight of the year is the Pride Festival and Pride Day (held in June) with the Parade making its way through the streets of Oxford. 

---

## Community resources

- [The Oxford LGBTQ+ Society](https://www.oulgbtq.org/) website provides a large number of resources suitable for both students and staff, including those related to LGBTQIA+ [welfare](https://www.oulgbtq.org/welfare.html) and [identities](https://www.oulgbtq.org/identity-resources.html). 

- [Oxford Pride](https://oxfordpride.uk/) also provide many resources and links to [charities](https://oxfordpride.uk/charities-and-resources/) and advice on how to access [help and support](https://oxfordpride.uk/help-and-support/). 

- [A short leaflet](https://edu.admin.ox.ac.uk/files/newlgbtstaffleafletpdf) with information for new LGBTQIA+ staff (provided by the EDU).

- The Oxford LGBTQ+ Society Trans and Non-Binary [info](https://www.oulgbtq.org/trans.html) and [recommended support](https://cliniq.org.uk/resources-during-the-pandemic/). 

- [T(ART) Productions](https://www.tartproductions.co.uk/): a not-for-profit queer community events company in Oxford specialising in arts, cabaret, and social events who run weekly meet-ups, organised through their [instagram](https://www.instagram.com/tartproductionsox) and [website](https://www.tartproductions.co.uk/upcoming-events).

- [University of Oxford Transgender Guidance](https://edu.admin.ox.ac.uk/transgender-guidance), with both helpful information and practical advice for trans and non-binary staff and students at Oxford.

- [Information](https://edu.admin.ox.ac.uk/sexual-orientation) about the University of Oxfords support for LGBTQIA+ staff and initiatives to promote a more inclusive workplace.

- [Welfare resources in Oxford](https://www.oulgbtq.org/welfareresources.html) for both student and staff.

- [Support services](https://www.oulgbtq.org/trans-support.html): if you or someone you know is struggling, there are people you can talk to.

- [EDU support](https://edu.admin.ox.ac.uk/support-available-to-staff) available to staff.

- [Harassment advice](https://edu.admin.ox.ac.uk/harassment-advice): information for staff and students on the University's response to Harassment and Bullying including support and advice.

- [Queer Oxford](https://queeroxford.info/queer-town/): celebrating 600+ years of LGBTQ+ history and heritage in the city Oscar Wilde called 'the capital of romance'.

- [LGBTQ+ nightlife venues in Oxford](https://theoxfordmagazine.com/queer-oxford-the-lgbtq-nightlife-venues-in-oxford/), including [The Jolly Farmers](https://www.jollyfarmers-oxford.co.uk/), Oxford’s (and one of the UK’s) oldest running LGBTQ+ pubs.


---

## LGBTQIA+ champions at Oxford and WIN

 [![Role Models](../../../docs/community/LGBTRoleModels.png)](https://edu.admin.ox.ac.uk/lgbt-role-models)   



