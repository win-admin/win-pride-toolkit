---
layout: default
title: FAQs
has_children: false
nav_order: 7
---


# Frequently Asked Questions
{: .fs-9 }

The answers below have been volunteered by members of WIN Pride - they are neither authoritative nor definitive, but are a starting point for wider conversations. If you don’t understand any of the terms used here, it may help to look at the Terms and Definitions page to get clued up.

Don’t see your question answered here? [Email us!](mailto:win-pride-queries@ndcn.ox.ac.uk) This page is a work in progress that we hope to develop over time by answering your questions; you can also submit any key information or tips you’ve found that you think would be helpful for others! We are also working on a solution to allow you to submit questions anonymously. More on this soon.

---

## Facts and Terminology

<details>
<summary>What percentage of people are LGBTQIA+?</summary>
<br>
This is a surprisingly nuanced question, since people may experience same-sex attraction but identify themselves as straight, and the safety of being LGBTQIA+ and their experiences (witnessing discrimination/hate-speech) may inform how open they are with their identity, and how comfortable they are with LGBTQIA+ labels. Interestingly, in the UK, 93.2% of people identify as heterosexual/straight but only 2.6% as LGB+ (all ages), and 4.1% refuse to comment. Nevertheless, global statistics suggest that the proportion is somewhere between 3-11% of the population.
<br>
For more detail see:
- UCLA, global and USA analysis, 2011: <a href="https://williamsinstitute.law.ucla.edu/publications/how-many-people-lgbt/"> How many people are lgbt?</a><br>
- UK Gov, latest UK report, 2017: <a href="https://www.ons.gov.uk/peoplepopulationandcommunity/culturalidentity/sexuality/bulletins/sexualidentityuk/2017">UK sexual identity statistics </a><br>
</details>

<details>
<summary>How many LGBTQIA+ people are out at work?</summary>
<br>
According to data from the IOP, RAS & RSC report ‘Exploring the workplace for LGBTQIA+ physical scientists’, between 14-44% of LGBTQIA+ people are out at work, where the percentage differs depending on how someone identifies.

<a href="../../../docs/faqs/stats-infographic.png">Infographic </a><br>

The full report can be found <a href="https://beta.iop.org/exploring-the-workplace-for-lgbtplus-physical-scientists"> here. </a><br> The report begins with an excellent summary of the main findings. Alternatively, the main infographic can be found <a href="https://d25f0oghafsja7.cloudfront.net/sites/default/files/2019-06/exploring-the-workplace-for-lgbtplus-physical-scientists-key-findings.pdf">here.</a><br>
</details>

<details>
<summary>What does LGBTQIA+ stand for?</summary>
<br>
You may wish to read our ‘Terms and Definitions' section to find out more. LGBTQIA+ stands for Lesbian Gay Bisexual Transgender Queer or Questioning Intersex and Asexual plus other non-cis/straight/normative identities. It is an acronym and umbrella term for all people who have a non-normative gender or sexuality, and additional letters that are often appended, for example, Q (queer or questioning) and A (asexual/aromantic).

Queer is also used as an umbrella term as an alternative to LGBTQIA+ (see <a href="https://www.pride.com/queer/2015/8/04/6-reasons-you-need-use-word-queer">reasons to use the word queer.</a><br>
</details>

---

## Guidance and Advice

<details>
<summary>Should I ask all my new students what their preferred pronouns are?</summary>
<br>
This is good practice, and even normalizing the introduction of yourself by including your preferred pronouns (such as to your new students) will invite others to reciprocate. “Hi I’m Dr X, pronouns she/her, …”
</details>

<details>
<summary>What should I do if I accidentally misgender someone?</summary>
<br>
People make mistakes, so don’t fret: just apologize, correct yourself, and move on with your lovely conversation. You should make an effort to use the correct pronouns in the future, but do not let this put you off talking to people. If you’re unsure on someone’s pronouns, then just ask “what are your pronouns?” - it is okay to ask, and better than accidentally misgendering.
</details>

<details>
<summary>What do I do if my colleague comes out to me?</summary>
<br>
Listen. This can be an incredibly daunting moment for the person who is coming out. Don’t make it more awkward by acting shocked, surprised, or making a big deal out of it - an understated pleasant reaction is often the most appreciated to calm their nerves and show that it’s not weird for you - you can smile and nod, and/or say something like “thanks for sharing.” Be an active listener.

It is a good idea to ask how you can support them, especially if you are their supervisor. They may not be comfortable being ‘out’ to other colleagues, or it may not bother them; everyone is different, so this is something to check when you ask about supporting them. If they are trans, then remember to ask what their pronouns are, and whether there is a different name that they would like you to use from now on.
</details>

---

## Advocacy and Allyship

<details>
<summary>Why are people putting pronouns in their email signature? Should I?</summary>
<br>
Yes, please! This normalizes discussions around gender and pronouns - if the majority of people state their pronouns (even cisgender folk), then it is much less awkward for those who do not identify with their sex assigned at birth or have experiences being misgendered, and it creates an easy and welcoming atmosphere to share pronouns. It’s also an important move to inclusivity that signals to others that you will accept their gender identity.
</details>

<details>
<summary>How can I signal to my colleagues that I am an ally?</summary>
<br>
Allyship is an active, continuous process of learning with specific criteria for engagement. Please refer to our 'Supporting Your Peers' page to learn more about what allyship meansg. Adding an LGBTQIA+ Ally badge to your email signature and your professional profile will let your colleagues know you are dedicated to empowering and supporting the LGBTQIA+ community.
</details>
