---
layout: default
title: Connect
has_children: false
nav_order: 7
---

# Contact Us
{: .fs-9 }

Connect with the LGBTQIA+ community at WIN via the details below.
{: .fs-6 .fw-300 }

---

## WIN members and external researchers

### Email

You are welcome to [email win-pride-queries@ndcn.ox.ac.uk](mailto:win-pride-queries@ndcn.ox.ac.uk) directly to discuss any issue relating to LGBTQIA+ at WIN, the community, or this repository.

---

## WIN members only

### WIN Community Slack ![slack](../../img/icon-slack.png)

Anyone affiliated with a WIN member Department is invited to [join our WIN Community Slack workspace](https://join.slack.com/t/oxford-win-community/signup). Our LGBTQIA+ community can be found on the LGBTQIA channel.

**Slack is where the conversation will be most active. If you're new to the Community, this should be your first contact point!**

The above [slack invite link](https://join.slack.com/t/oxford-win-community/signup) will accept email addresses from any of the University of Oxford domains listed below. If you think your domain should be listed here, or you are a WIN member without an email address at one of these domains, please email [WIN administration](mailto:win-admin@ndcn.ox.ac.uk) for further help.

#### WIN member domains (alphabetically)
- [bdi.ox.ac.uk](https://www.bdi.ox.ac.uk)
- [eng.ox.ac.uk](https://eng.ox.ac.uk)
- [fmrib.ox.ac.uk](https://www.win.ox.ac.uk/about/locations/fmrib)
- [ndcn.ox.ac.uk](https://www.ndcn.ox.ac.uk)
- [ohba.ox.ac.uk](https://www.win.ox.ac.uk/about/locations/ohba)
- [paediatrics.ox.ac.uk](https://www.paediatrics.ox.ac.uk)
- [psy.ox.ac.uk](https://www.psy.ox.ac.uk)
- [psych.ox.ac.uk](https://www.psych.ox.ac.uk)
- [win.ox.ac.uk](https://www.win.ox.ac.uk)

