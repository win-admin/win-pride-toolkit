---
layout: default
title: Activities
has_children: false
nav_order: 5
---


# LGBTQIA+ Activities Page

## Overview
Welcome to our Activities page! Here, you can explore the various events and initiatives we have organized for and about the LGBTQIA+ community. 

---

## Past Activities

We take *pride* in our past events. Below are some highlights from our previous activities.

### [Annual Pride Quiz]
- **Date:** [06/2024]
- **Description:** During PRIDE month, we host a pride themed quiz open to all members and their family. This is not an educational quiz though, it's just meant to have fun!
- **Last Quiz** Find our last Quiz here! (not here)

- **Pictures from the event**

tbc
---

## Recurring Activities

### Pride Quiz
Every year we host the Pride quiz during Pride month! 
- **Frequency:** Annually (duh!)
- **Next Session:** Next pride month (in JUNE, duh!)

### LGBTQ+ reading group
The WIN book club sometimes host an LGBTQAI+ themed event, join their slack channel and stay tuned!
![books](../../assets/lgbtqai_books.jpg)

---

## How to Organize or Submit Ideas

Please! We need you to make community! If you have an idea for an activity or would like to organize one, here’s how you can get involved:

### Organize an Event
Contact us on the WIN-Community slack channel with your idea, including the event's goals, target audience, and logistical details.
Please include the date of your event, the help needed, and a detailed description of it (if you have one :-))

If you have a suggestion for a recurring activity or a one-time event but aren't ready to organize it yourself, we still want to hear from you! We are ready to help/support your ideas!

*Together, we can build a community!*

---

## Contact Us
For any questions, or if you need assistance with the event organization process, please [contact us](link-to-contact-page).

---

Thank you for being an active part of our community. Your participation helps us create a more inclusive and supportive environment for everyone.
