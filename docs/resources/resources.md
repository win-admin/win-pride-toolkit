---
layout: default
title: Resources
has_children: false
nav_order: 2
---

# Resources
{: .fs-9 }

A few of our favourite resources to get you started.
{: .fs-6 .fw-300 }

There are tonnes of great resources out there. Here we provide links to a few of our favourites to get you started. If there is a particular resource that you would like to
see listed here, let us know: [win-pride-queries@ndcn.ox.ac.uk](mailto:win-pride-queries@ndcn.ox.ac.uk)

![stats](../../../docs/resources/stats.png)

*adapted from IOP https://beta.iop.org/exploring-the-workplace-for-lgbtplus-physical-scientists*

---

## Resources
Click on the headers to find articles, reports, videos and infographics related to the themes.

<details>
<summary><b>Terms and Definitions Glossary</b></summary>
<br>
There are different aspects to someone’s gender & sexual identity. <a href="https://open.win.ox.ac.uk/pages/win-admin/win-pride-toolkit/docs/terms-and-definitions/terms-and-definitions/">This page​</a> highlights some common terms and definitions, to help you feel more informed and understand when people have different identities.<br>
<br>

</details>


<details>
<summary><b>LGBTQIA+ in the UK</b></summary>
<br>
<b>National LGBT Survey, UK government.</b> <a href="https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/722314/GEO-LGBT-Survey-Report.pdf">Summary report.</a><br>
A survey of over 108,000 LGBTQIA+ people over 16 in the UK; they continue to face significant barriers to full participation in public life. The headline findings focus on life experience, safety, health, education and employment.<br>
<br>

<b>LGBTQIA+ in Britain - University report, Stonewall.</b> <a href="https://www.stonewall.org.uk/lgbt-britain-university-report">Report.</a><br>
A report on UK University student experiences for LGBTQIA+ individuals, highlighting lacking support, and harrowing rates of discrimination from students & staff, particularly for trans students. Despite positive action and progress of support by many universities, there is clearly much more to be done.<br>
<br>

</details>



<details>
<summary><b>LGBTQIA+ in Science</b></summary>
<br>
<b>LGBTQ scientists are still left out, Nature Comment (2018).</b> <a href="https://www.nature.com/articles/d41586-018-05587-y">Main Report.</a> <a href="https://d25f0oghafsja7.cloudfront.net/sites/default/files/2019-06/exploring-the-workplace-for-lgbtplus-physical-scientists-key-findings.pdf">Infographic of main findings.</a><br>
Prof. Jon Freeman (NYU) summarizes key issues facing LGBTQIA+ students/postdocs & makes the case for integrating “LGBTQIA+” into larger diversity and inclusion initiatives already underway at most universities.<br>
<br>

<b>The workplace for LGBTQIA+ scientists, IOP, RAS & RSC (2019).</b> <a href="https://d25f0oghafsja7.cloudfront.net/sites/default/files/2019-06/exploring-the-workplace-for-lgbtplus-physical-scientists_1.pdf">Report.</a><br>
A summary of findings from the Institute of Physics, Royal Astronomical Society and Royal Society of Chemistry LGBTQIA+ member-led steering group investigation of the needs of LGBTQIA+ physical scientists in the UK & Ireland. Guidance on “next steps” for improving workplace inclusivity is highlighted.<br>
<br>

<b>Documenting diversity: the experiences of LGBTQ+ doctoral researchers in the UK, International Journal of Doctoral Studies (2019).</b> <a href="https://unioxfordnexus-my.sharepoint.com/personal/clne0244_ox_ac_uk/Documents/Microsoft%20Teams%20Chat%20Files/IJDSv14p403-430English4721.pdf">Paper.</a><br>
A questionnaire-based study that captures the diverse experiences of doctoral researches in the UK that identify as LGBTQIA+. Results offer an assessment of “campus climate” for doctoral candidates with suggestions for how institutions can adapt & improve.<br>
<br>

<b>A model of queer STEM identity in the workplace, Journal of Homosexuality (2019).</b> <a href="https://www.tandfonline.com/doi/full/10.1080/00918369.2019.1610632">Paper.</a><br>
An inquiry into the extent to which LGBTQIA+ students, faculty and staff in STEM negotiate their identity based on workplace assumptions about gender and sexuality. Opportunities for improving inclusivity are discussed.<br>
<br>

</details>


<details>
<summary><b>Trans and non-binary inclusion</b></summary>
<br>
<b>Building trans-inclusive scientific workplaces, by the Royal Society of Chemistry </b> <a href="https://www.rsc.org/globalassets/22-new-perspectives/talent/inclusion-and-diversity/resources/lgbt-toolkit/building-trans-inclusive-workplaces.pdf"> Report.</a><br> 
This practical resource is made up of three sections, aiming at trans people, individuals looking to practice trans allyship and employers and managers seeking to make workplaces more trans-inclusive. <br>
<br>

<b>Inclusivity: supporting BAME trans people, by Sabah Choudrey and the Gender Identity Research and Education Society</b>  <a href="https://www.gires.org.uk/wp-content/uploads/2016/02/BAME_Inclusivity.pdf"> Report.</a><br> 
A 28-page guide with practical tips, guidance, and resources dedicated to supporting the Black, Asian & Minority Ethnic (BAME) trans population of Britain. <br>
<br>

<b>Beyond XX and XY, Scientific American (2017)</b>  <a href="../../../docs/resources/resources-to-be-added/BeyondXXandXY.pdf"> Infographic.</a><br> 
A host of factors figure into whether someone is female, male, or somewhere in-between. <br>
<br>

<b>Supporting transgender staff, University of Nottingham (2019)</b>  <a href="../../../docs/resources/BeyondXXandYY.pdf](https://www.nottingham.ac.uk/hr/guidesandsupport/equalityanddiversitypolicies/documents/supporting-transgender-staff.pdf"> Report.</a><br> 
<br>

</details>



<details>
<summary><b>Leadership & Role Models</b></summary>
<br>
<b>Stepping up to be a role model for LGBTQ inclusion in science, Nature Career Column (2019).</b> <a href="https://www.nature.com/articles/d41586-019-01146-1">Article.</a><br>
Neil Reavey talks about personal experiences and describes how scientists can support or lead initiatives in their organizations.<br>
<br>

<b>Hidden diversity steps out from the closet, RSC Opinions.</b> <a href="https://www.rsc.org/news-events/opinions/2018/jul/david-smith-lgbtstem-day/">Article.</a><br>
David Smith shares his thoughts on why the scientific community has work to do in supporting current and aspiring LGBTQIA+ scientists.<br>
<br>

</details>



<details>
<summary><b>LGBTQIA+ Speakers</b></summary>
<br>
Here are some talks from LGBTQIA+ individuals which focus on Equality Diversity and Inclusion, including LGBTQIA+ issues and beyond. Some of these have been hosted by the WIN and are available internally, others are public facing:<br>
<br>
<b>Dr. Lilly Hunt</b><br>
LGBTStem Day: "Equality, Diversity and Inclusion In Science and Health Research"<br>
<a href="https://ox.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=7fcff060-efbf-4be8-9574-aa8100d745b8">Click here to watch on Panopto.</a><br>
<br>

<b>Dr. Lenna Cumberbatch</b><br>
"What kind of equality, diversity and inclusion (EDI) do you want? Understanding and selecting an inclusive approach"<br>
<a href="https://ox.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=df604a2a-d273-4b3c-ab0e-abc200c773cd">Click here to watch on Panopto.</a><br>
<br>

<b>Dr. Clara Barker, ft. Dr. Izzy Jayasinghe and Guests</b><br>
Oxford University LGBTQIA+ Advisory Grop Microtalk: "I'm LGBT and..."<br>
<a href="https://www.youtube.com/watch?v=rPBMwOCIQ9k">Click here to watch on YouTube.</a><br>
<br>

<b>Dr. Ben Britton</b><br>
Imperial College London LGBT History Month Talk: "No sexuality please, we're scientists"<br>
<a href="https://www.youtube.com/watch?v=NEqfP1diyes">Click here to watch on YouTube.</a><br>
<br>

<b>University of Oxford LGBTQIA+ history month lectures</b><br>
<a href="https://edu.admin.ox.ac.uk/podcasts#collapse1186026">Available here.</a><br>
<br>


</details>


<details>
<summary><b>Other Resources</b></summary>
<br>
<b>Party and protest: the radical history of gay liberation.</b> <a href="https://www.theguardian.com/world/2020/jun/25/party-and-protest-lgbtq-radical-history-gay-liberation-stonewall-pride">Article.</a><br>
Stonewall and Pride, The Guardian (2020).<br>
<br>

<b>Delivering through diversity: how inclusion and diversity impact growth and business performance.</b> <a href="https://www.mckinsey.com/~/media/mckinsey/business%20functions/organization/our%20insights/delivering%20through%20diversity/delivering-through-diversity_full-report.ashx">Report.</a><br>
McKinsey & Company (2018).<br>
<br>

</details>

---

## Have you ever thought about...?
There are some specific issues that your LGBTQIA+ colleagues may face in the workplace, travelling for work, and even applying for grants. Below are some examples outlining these issues and resources that you may find helpful.<br>

<details>
<summary><b>Experiences as an LGBTQIA+ scientist</b></summary>

<b>’No sexuality please, we’re scientists.’</b> <a href="https://www.chemistryworld.com/opinion/no-sexuality-please-were-scientists/7197.article">Article.</a><br>
Chemistry World (2014).<br>
<br>

<b>Coming out in STEM: Factors affecting retention of sexual minority STEM students.</b> <a href="https://advances.sciencemag.org/content/4/3/eaao6373">Article.</a><br>
Science Advances (2018).<br>
<br>

<b>'Sexuality isn't irrelevant to the profession but is part of the individuals who make up the workforce.'</b> <a href="https://twitter.com/UKRI_News/status/1277198737929048064">Video.</a><br>
UKRI (2020).<br>
<br>

</details>



<details>
<summary><b>LGBTQIA+ Travel Safety</b></summary>
<br>
LGBTQIA+ scientists face different levels of persecution and discrimination worldwide, impacting career moves to other countries, fieldwork, research visits, and conferences. Alluded to in the “no sexuality please” article. There are many issues and no one summary, but this <a href="https://twitter.com/LGBT_Physics/status/1083728673830825986">twitter thread</a> from a panel discussion at LGBTQIA+ STEMinar 2019 highlights some of the widely unconsidered risks.<br>
<br>

<a href="https://ilga.org/maps-sexual-orientation-laws"><b>A map of sexual orientation laws across the world.</b></a><br>
IGLA world<br>
<br>

<a href="https://www.gov.uk/guidance/lesbian-gay-bisexual-and-transgender-foreign-travel-advice"><b>LGBTQIA+ foreign travel advice.</b></a><br>
UK government <br>
<br>

</details>



<details>
<summary><b>LGBTQIA+ Barriers to Funding</b></summary>
<br>
<b>Barriers LGBTQIA+ people face in the research funding process.</b> <a href="https://www.tigerinstemm.org/resources/barriers-to-funding">Report.</a><br>
Tigers in STEM.<br>
<br>

</details>

---

## Other websites and social media “follows” to check out:

[![Tiger in STEMM](../../../docs/resources/tigerstemm.png)](https://www.tigerinstemm.org/)
[![edis](../../../docs/resources/edis.png)](https://edisgroup.org/)
[![500 Queer Scientists](../../../docs/resources/500qs.png)](https://500queerscientists.com/)
[![Stonewall](../../../docs/resources/stonewall.png)](https://www.stonewall.org.uk/)
[![House of STEM](../../../docs/resources/houseofstem.png)](https://houseofstem.org/)
[![Pride in STEM](../../../docs/resources/prideinstem.png)](https://prideinstem.org/)

